Want to install a BindTuning theme in Orchard but don't know how to get started? Looking for instructions on how to upload BindTuning's demo content? You've come to the right place!

In this user guide we cover all the basics of using a BindTuning theme in Orchard, from installing your theme, uploading a demo content, upgrading your theme and more. 

From the left menu go through our detailed instructions and your theme will be up, running and looking fabulous in no time. 