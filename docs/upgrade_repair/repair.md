**Warning:** ALWAYS make a backup of all of your theme assets, including CSS files, master pages, page layouts,... Repairing the theme will remove all custom changes you have applied previously.

**Important:** Before upgrading or repairing your theme, clean your browser cache.

Repairing your theme is as easy as preparing yourself a cup of coffee. The only thing you need to do is install the theme package again and that's it.

Follow these steps: 

1. Open your Orchard admin area; 
2. On the left menu, **click on *Themes***; 
	![repair_1](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uninstall_1.png)
3. Open the *Packages* folder and **click on *Install a theme from your computer***;
4. Click on *Choose File* and **search for your theme package**, *yourtheme*.ORX1.nupkg; 
5. Now click on "Install";


And that's it! 
