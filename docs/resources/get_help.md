### Help Center

Our Help Center is a great resource when you need a quick solution. Go through the useful articles or talk with the community and get your answer from fellow users. 

Visit <a href="https://support.bindtuning.com/hc" target="_blank">https://support.bindtuning.com/hc</a>.


### Standard Support

Standard Support is available to address the following issues within the first 60 days of purchase:

- Errors during product installation
- Product not working properly, or as described, or as documented within user guide
- General questions, help, advice on questions directly related to the product.

All support requests are closely monitored and are typically addressed within 24 to 48 hours. You can submit your ticket at <a href="http://bindtuning.com/support" target="_blank">bindtuning.com/support</a>.


### Premium Support

Premium Support is offered for additional requirements beyond what is covered in Standard Support, including:

- Modifications, changes or further customizations to the product
- Themes/Modules installation
- General Consulting / Custom Development of DotNetNuke, SharePoint, Orchard, Kentico or Umbraco Themes
- Standard support beyond 60 days of purchase
- Fixes required outside of the normally scheduled patches and releases.
- Priority support over standard support

For information on our Premium Support, visit our article <a href="https://support.bindtuning.com/hc/en-us/articles/205172655-How-does-premium-support-work-" target="_blank">How does premium support work?</a>.

### Suggest a feature

Have an idea for a new feature or experience at BindTuning.com? Visit My BindTuning idea where you can leave your idea and comment and vote on other ideas. 

Visit <a href="http://bind.uservoice.com/" target="_blank">bind.uservoice.com</a>.
