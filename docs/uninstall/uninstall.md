Go through these steps if you want to completely uninstall and delete your BindTuning theme. 

<a name="beforeuninstalling"></a>
###Before uninstalling 

**Open your admin area** and from the left menu **click on "Themes"**. 

You should be able to find your theme there. Go through your list of themes available and **set one as your current theme**. 


![uninstall_1](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uninstall_1.gif)

Now you are ready to go.


 
<a name="uninstallingthetheme"></a>
###Uninstalling the theme

**Warning:** Uninstalling the theme will permanently delete the theme from your website and all its files. 

Uninstalling a BindTuning theme from Orchard website is pretty simple and straightforward. 

Still inside your Themes area, click on "Uninstall". If a message comes through saying "Are you sure you want to remove this element?", click "Ok". 


![uninstall_3](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/uninstall_3.png)


That's it. Your theme is now uninstalled and deleted from your website.



