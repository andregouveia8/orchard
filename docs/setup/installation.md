<a name="installingthetheme"></a>
###Installing the theme
Inside your theme's package you will find a .nupkg file which you will be using for installing the theme. 

Lets get your fresh downloaded theme into your website:

1. **Login** to your Orchard website using an admin account;
2. Inside the dashboard, on the left hand menu, **click on "Themes"**; 
3. **Click on "Install a theme from your computer"**; 
4. Click on "Choose File" and **search for your theme package**, *yourtheme*.ORX1.nupkg;
5. Now **click on "Install"**;
	![installation_3](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/installation_3.png)  
6. And that's it. You should get a success message. 
	![installation_4](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/installation_4.png)  
	
	


<a name="applyingthetheme"></a>
###Applying the theme
**If you want to see the theme applied to your website** you will need to set it as your current theme.

**Still inside the "Themes" page**, on Orchard Dashboard, **click on "Set Current"** and the theme will be applied to your website. 

![installation_5](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/installation_5.png) 

![installation_6](https://bitbucket.org/bindtuningteam/orchard-docs/wiki/images/installation_6.png) 


Installation done! On to the next chapter where we walk you through uploading BindTuning's demo content into your website. 